"Ejercicio 6"

from mysoundsin import SoundSin
from mysound import Sound  # Agrega esta línea
import unittest


class TestSoundSin(unittest.TestCase):
    def test_soundsin_creation(self):
        # Prueba la creación de un objeto SoundSin
        duration = 1.0
        frequency = 440
        amplitude = 10000
        sound_sin = SoundSin(duration, frequency, amplitude)
        self.assertIsInstance(sound_sin, SoundSin)

    def test_soundsin_inherits_from_sound(self):
        # Prueba que un objeto SoundSin hereda de Sound
        duration = 1.0
        frequency = 440
        amplitude = 10000
        sound_sin = SoundSin(duration, frequency, amplitude)
        self.assertIsInstance(sound_sin, SoundSin)
        self.assertIsInstance(sound_sin, Sound)

    def test_soundsin_buffer(self):
        # Prueba que el buffer de SoundSin contiene
        # datos de una señal sinusoidal
        duration = 1.0
        frequency = 440
        amplitude = 10000
        sound_sin = SoundSin(duration, frequency, amplitude)

        # Verifica que al menos una muestra en el buffer
        # no sea cero (indicando una señal generada)
        self.assertTrue(any(sound_sin.buffer))
