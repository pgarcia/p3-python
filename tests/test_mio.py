"Ejercicio 4"

import unittest
from mysound import Sound


class TestSound(unittest.TestCase):

    def test_values(self):
        sound = Sound(2)
        sound.sin(440, 10000)

        # Verifica que todos los valores estén
        # dentro del rango esperado
        self.assertTrue(all
                        (-Sound.max_amplitude <= x <= Sound.max_amplitude
                         for x in sound.buffer))


if __name__ == '__main__':
    unittest.main()
