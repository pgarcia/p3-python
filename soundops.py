"Ejercicio 7"

from mysound import Sound


def soundadd(s1: Sound, s2: Sound) -> Sound:
    """
    Suma dos sonidos (objetos de la jerarquía Sound) y
    devuelve un nuevo objeto Sound resultante.

    :param s1: El primer sonido a sumar
    :param s2: El segundo sonido a sumar
    :return: Un nuevo objeto Sound que
    es la suma de s1 y s2
    """
    # Determina la duración del sonido resultante como
    # la duración del sonido más largo
    duration = max(s1.duration, s2.duration)

    # Calcula el número de muestras en el sonido resultante
    nsamples = int(duration * s1.samples_second)

    # Crea un nuevo objeto Sound para el sonido resultante
    result_sound = Sound(duration)

    # Suma las muestras de s1 y s2 hasta que una de las secuencias termine
    for i in range(min(len(s1.buffer), len(s2.buffer))):
        result_sound.buffer[i] = s1.buffer[i] + s2.buffer[i]

    # Si s1 tiene más muestras que s2, copia las muestras restantes de s1
    if len(s1.buffer) > len(s2.buffer):
        for i in range(len(s2.buffer), len(s1.buffer)):
            result_sound.buffer[i] = s1.buffer[i]

    # Si s2 tiene más muestras que s1, copia las muestras restantes de s2
    elif len(s2.buffer) > len(s1.buffer):
        for i in range(len(s1.buffer), len(s2.buffer)):
            result_sound.buffer[i] = s2.buffer[i]

    return result_sound
